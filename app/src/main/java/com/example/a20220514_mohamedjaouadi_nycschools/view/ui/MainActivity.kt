/*
 *
 *  * Mohamed Jaouadi
 *  * 
 *
 */

package com.example.a20220514_mohamedjaouadi_nycschools.view.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.a20220514_mohamedjaouadi_nycschools.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction()
            .add(R.id.main_activity_fragment, MainFragment::class.java, null)
            .commit()
    }


}