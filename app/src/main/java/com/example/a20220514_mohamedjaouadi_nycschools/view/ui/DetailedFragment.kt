/*
 *
 *  * Mohamed Jaouadi
 *  * 
 *
 */

package com.example.a20220514_mohamedjaouadi_nycschools.view.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.a20220514_mohamedjaouadi_nycschools.R
import com.example.a20220514_mohamedjaouadi_nycschools.service.model.SATData
import com.example.a20220514_mohamedjaouadi_nycschools.viewmodel.SATViewModel

class DetailedFragment:Fragment() {

    private lateinit var textMath:TextView
    private lateinit var textRead:TextView
    private lateinit var textWrite:TextView
    private lateinit var viewModel: SATViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val textSchoolName = view.findViewById<TextView>(R.id.text_school_name)
        val textOverview = view.findViewById<TextView>(R.id.text_overview)
        textSchoolName.text = arguments?.getString("school_name")
        textOverview.text =  arguments?.getString("overview_paragraph")

        textMath = view.findViewById(R.id.text_math)
        textRead = view.findViewById(R.id.text_read)
        textWrite = view.findViewById(R.id.text_write)

        viewModel = ViewModelProvider(this)[SATViewModel::class.java]
        viewModel.SATList.observe(viewLifecycleOwner){
            setSatData(it)
        }
        val dbn = arguments?.getString("dbn")
        viewModel.getSatDetails(dbn?:"")
    }

    private fun setSatData(data:List<SATData>){
        if(data.isNotEmpty()){
            textMath.text = data[0].sat_math_avg_score
            textRead.text = data[0].sat_critical_reading_avg_score
            textWrite.text = data[0].sat_writing_avg_score
        }
    }
}