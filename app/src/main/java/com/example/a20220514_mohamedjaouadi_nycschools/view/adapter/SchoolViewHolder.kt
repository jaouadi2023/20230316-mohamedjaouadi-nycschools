/*
 *
 *  * Mohamed Jaouadi
 *  * 
 *
 */

package com.example.a20220514_mohamedjaouadi_nycschools.view.adapter

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220514_mohamedjaouadi_nycschools.R
import com.example.a20220514_mohamedjaouadi_nycschools.service.model.SchoolData
import com.example.a20220514_mohamedjaouadi_nycschools.view.ui.ItemClickHandler

class SchoolViewHolder(view:View, private val clickHandler: ItemClickHandler):RecyclerView.ViewHolder(view) {
    fun bind(item: SchoolData){
        val textSchoolName = itemView.findViewById<TextView>(R.id.item_school_name)
        val textNumber = itemView.findViewById<TextView>(R.id.item_number)
        textSchoolName.text = item.school_name
        textNumber.text = item.phone_number
        itemView.setOnClickListener {
            clickHandler.onItemClick(item)
        }
    }
}