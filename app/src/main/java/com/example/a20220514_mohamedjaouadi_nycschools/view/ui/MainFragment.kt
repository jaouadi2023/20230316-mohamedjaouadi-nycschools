/*
 *
 *  * Mohamed Jaouadi
 *  * 
 *
 */

package com.example.a20220514_mohamedjaouadi_nycschools.view.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220514_mohamedjaouadi_nycschools.R
import com.example.a20220514_mohamedjaouadi_nycschools.service.model.SchoolData
import com.example.a20220514_mohamedjaouadi_nycschools.view.adapter.SchoolAdapter
import com.example.a20220514_mohamedjaouadi_nycschools.viewmodel.SchoolViewModel

class MainFragment:Fragment(), ItemClickHandler{

    private lateinit var adapter: SchoolAdapter
    private lateinit var viewModel: SchoolViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recycler = view.findViewById<RecyclerView>(R.id.recyclerview)
        val progressbar = view.findViewById<ProgressBar>(R.id.progress_bar)
        progressbar.visibility = View.VISIBLE

        //setAdapter
        adapter = SchoolAdapter(this)
        recycler.adapter = adapter
        recycler.layoutManager = LinearLayoutManager(requireContext())

        viewModel = ViewModelProvider(this)[SchoolViewModel::class.java]
        viewModel.schoolList.observe(viewLifecycleOwner){
            progressbar.visibility = View.GONE
            adapter.submitList(it)
        }
        viewModel.getAllSchool()
    }

    override fun onItemClick(item: SchoolData) {
        val fragmentTransaction: FragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
        val newFragment = DetailedFragment()
        val args = Bundle()
        args.putString("dbn", item.dbn)
        args.putString("school_name", item.school_name)
        args.putString("overview_paragraph", item.overview_paragraph)
        newFragment.arguments = args

        fragmentTransaction.replace(
            (requireView().parent as ViewGroup).id, newFragment
        )
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

}

interface ItemClickHandler{
    fun onItemClick(item:SchoolData)
}