/*
 *
 *  * Mohamed Jaouadi
 *  * 
 *
 */

package com.example.a20220514_mohamedjaouadi_nycschools.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.a20220514_mohamedjaouadi_nycschools.R
import com.example.a20220514_mohamedjaouadi_nycschools.service.model.SchoolData
import com.example.a20220514_mohamedjaouadi_nycschools.view.ui.ItemClickHandler

class SchoolAdapter(private val clickHandler: ItemClickHandler):ListAdapter<SchoolData, SchoolViewHolder>(DiffCallBack) {
    object DiffCallBack: DiffUtil.ItemCallback<SchoolData>(){


        override fun areItemsTheSame(oldItem: SchoolData, newItem: SchoolData): Boolean {
            return oldItem.dbn == newItem.dbn
        }

        override fun areContentsTheSame(oldItem: SchoolData, newItem: SchoolData): Boolean {
            return oldItem.dbn == newItem.dbn
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        return SchoolViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false),
            clickHandler
        )
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.bind((getItem((position))))
    }
}