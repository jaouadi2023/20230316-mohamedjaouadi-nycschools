/*
 *
 *  * Mohamed Jaouadi
 *  * 
 *
 */

package com.example.a20220514_mohamedjaouadi_nycschools.service.repository

import com.example.a20220514_mohamedjaouadi_nycschools.service.model.SATData
import com.example.a20220514_mohamedjaouadi_nycschools.service.model.SchoolData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val BASE_URL = "https://data.cityofnewyork.us/resource/"
class ServiceSchoolRepository :SchoolRepository{
    private var api:SchoolApi
    init {
        val retrofit = Retrofit.Builder().baseUrl(BASE_URL).
        addConverterFactory(GsonConverterFactory.create())
        api = retrofit.build().create(SchoolApi::class.java)

    }

    override fun getAllSchool(): Flow<List<SchoolData>> {
        return flow { emit(api.getAllSchool()) }
    }

    override fun getSatDetails(dbn:String): Flow<List<SATData>> {
        return flow { emit(api.getSATData(dbn)) }
    }

}