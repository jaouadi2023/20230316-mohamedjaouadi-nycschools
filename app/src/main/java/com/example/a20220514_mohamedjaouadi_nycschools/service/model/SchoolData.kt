/*
 *
 *  * Mohamed Jaouadi
 *  * 
 *
 */

package com.example.a20220514_mohamedjaouadi_nycschools.service.model

import java.io.Serializable

data class SchoolData (
    val dbn:String,
    val school_name:String,
    val overview_paragraph:String,
    val phone_number:String,
    val website:String,
    val latitude:Double,
    val longitude:Double
) : Serializable