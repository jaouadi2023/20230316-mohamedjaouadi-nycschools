/*
 *
 *  * Mohamed Jaouadi
 *  * 
 *
 */

package com.example.a20220514_mohamedjaouadi_nycschools.service.model

data class SATData (
    val dbn:String,
    val sat_critical_reading_avg_score:String,
    val sat_math_avg_score:String,
    val sat_writing_avg_score:String
)