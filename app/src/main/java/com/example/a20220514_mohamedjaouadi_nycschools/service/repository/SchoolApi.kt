/*
 *
 *  * Mohamed Jaouadi
 *  * 
 *
 */

package com.example.a20220514_mohamedjaouadi_nycschools.service.repository

import com.example.a20220514_mohamedjaouadi_nycschools.service.model.SATData
import com.example.a20220514_mohamedjaouadi_nycschools.service.model.SchoolData
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolApi {
    @GET("s3k6-pzi2.json")
    suspend fun getAllSchool():List<SchoolData>

    @GET("f9bf-2cp4.json")
    suspend fun getSATData(@Query("dbn") dbn:String):List<SATData>
}