/*
 *
 *  * Mohamed Jaouadi
 *  * 
 *
 */

package com.example.a20220514_mohamedjaouadi_nycschools.service.repository

import com.example.a20220514_mohamedjaouadi_nycschools.service.model.SATData
import com.example.a20220514_mohamedjaouadi_nycschools.service.model.SchoolData
import kotlinx.coroutines.flow.Flow

interface SchoolRepository {
    fun getAllSchool():Flow<List<SchoolData>>
    fun getSatDetails(dbn:String):Flow<List<SATData>>
}