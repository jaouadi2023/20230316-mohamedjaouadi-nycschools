/*
 *
 *  * Mohamed Jaouadi
 *  * 
 *
 */

package com.example.a20220514_mohamedjaouadi_nycschools.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220514_mohamedjaouadi_nycschools.service.repository.SchoolRepository
import com.example.a20220514_mohamedjaouadi_nycschools.service.repository.ServiceSchoolRepository
import com.example.a20220514_mohamedjaouadi_nycschools.service.model.SchoolData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

class SchoolViewModel :ViewModel() {
    val schoolList = MutableLiveData<List<SchoolData>>()
    var repo:SchoolRepository = ServiceSchoolRepository()
    fun getAllSchool(){
        viewModelScope.launch {
            repo.getAllSchool().flowOn(Dispatchers.IO).catch {
                it.localizedMessage
            }.collect{
                t-> schoolList.postValue(t.map{ it})
            }
        }
    }
}
