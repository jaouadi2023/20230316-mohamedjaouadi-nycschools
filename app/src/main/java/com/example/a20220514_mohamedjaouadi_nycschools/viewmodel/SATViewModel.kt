/*
 *
 *  * Mohamed Jaouadi
 *  * 
 *
 */

package com.example.a20220514_mohamedjaouadi_nycschools.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220514_mohamedjaouadi_nycschools.service.model.SATData
import com.example.a20220514_mohamedjaouadi_nycschools.service.repository.SchoolRepository
import com.example.a20220514_mohamedjaouadi_nycschools.service.repository.ServiceSchoolRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

class SATViewModel : ViewModel() {
    val SATList = MutableLiveData<List<SATData>>()
    var repo: SchoolRepository = ServiceSchoolRepository()
    fun getSatDetails(dbn:String){
        viewModelScope.launch {
            repo.getSatDetails(dbn).flowOn(Dispatchers.IO).catch {
                it.localizedMessage
            }.collect{
                    t-> SATList.postValue(t.map{ it})
            }
        }
    }
}